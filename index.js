// console.log("Hello World!")

// Dummy Database
let posts = [];
let count = 1;

// Add Post Data
document.querySelector('#form-add-post').addEventListener("submit", (e) => {

	// this will prevent the webpage from reloading/refreshing
	e.preventDefault();

	// acts like the schema or model of the document
	posts.push({
		id: count,
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	});

	// this will increment the id value 
	count++;

	// function call to show all posts
	showPost(posts);
	alert('Succesfully Added')
});

// Show Post
const showPost = (posts) => {

	let postEntries = '';

	posts.forEach((post) => {

		postEntries += `
		<div id="post-${post.id}">
			<h3 id="post-title-${post.id}">${post.title}</h3>
			<p id="post-body-${post.id}">${post.body}</p>
			<button onclick="editPost('${post.id}')">Edit</button>
			<button onclick="deletePost('${post.id}')">Delete</button>
		</div>
		`;
	});

	document.querySelector('#div-post-entries').innerHTML = postEntries
};

// Edit Post
const editPost = (postId) => {

	let title = document.querySelector(`#post-title-${postId}`).innerHTML ;
	let body = document.querySelector(`#post-body-${postId}`).innerHTML;

	document.querySelector('#txt-edit-id').value = postId;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
};

// Delete Post
const deletePost = (postId) => {

	posts = posts.filter(post => {

		if(post.id.toString() !== postId) {
			return post;
		};
	});

	document.querySelector(`#post-${postId}`).remove();
};

// Update Post
document.querySelector("#form-edit-post").addEventListener('submit', e => {

	e.preventDefault();

	for(let i = 0; i < posts.length; i++) {
		// The values post[i].id is a NUMBER while document.querySelector("#txt-edit-id").value is a STRING
		// Therefore, it is necessary to convert the NUMBER to a STRING

		if(posts[i].id.toString() === document.querySelector(`#txt-edit-id`).value) {
			posts[i].title = document.querySelector('#txt-edit-title').value
			posts[i].body = document.querySelector('#txt-edit-body').value

			showPost(posts);
			alert("Succesfully Updated!");

			break;
		}

		
	};
});
